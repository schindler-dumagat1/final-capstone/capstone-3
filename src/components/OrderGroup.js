import React, { Fragment, useState, useEffect } from 'react'

import OrderCard from '../components/OrderCard'

import './OrderGroup.css'

const OrderGroup = ({groupProp}) => {

	const [items, setItems] = useState([])
	const [total, setTotal] = useState(0)

	useEffect(() => {
		let accumulated = 0
		setItems(groupProp.map(item => {
			accumulated += item.subTotal;
			return(<OrderCard key={groupProp.indexOf(item)} cardProp={item} />)
		}))
		setTotal(accumulated);

	}, [])

	return(
		<Fragment>
			<div className='pads'>
				<h5 className='data-time-space'>
					<span>Date: {groupProp[0].purchasedOn.slice(0, 10)}</span>
					<span>Time: {groupProp[0].purchasedOn.slice(12, 19)}</span>
				</h5>
				{items}
				<h4>Total Price: &#8369;{total}</h4>
			</div>
		</Fragment>
	)

}

export default OrderGroup