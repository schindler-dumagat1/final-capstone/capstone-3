import React, { Fragment } from 'react'
import './OrderCard.css'

const OrderCard = ({cardProp}) => {

	const { name, description, price, quantity, subTotal, purchasedOn } = cardProp;
	
	return(
		<Fragment>
			<div className='pads2'>
				<h1>{name}</h1>
				<p>{description}</p>
				<h6>Price: &#8369;{price}</h6>
				<h6>Quantity: {quantity}</h6>
				<h5>Subtotal: &#8369;{subTotal}</h5>
			</div>
		</Fragment>
	)

}

export default OrderCard