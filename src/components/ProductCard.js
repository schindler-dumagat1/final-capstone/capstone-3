//React Main Package
import React, { Fragment, useState, useEffect, useContext } from 'react';

//React Bootstrap Package
import Button from 'react-bootstrap/Button';

//React Router Package
import { Link } from 'react-router-dom';

//Pages
import ProductView from '../pages/ProductView';

//Global User Context
import UserContext from '../components/UserContext';

import './ProductCard.css'



const ProductCard = ({productProp}) => {
	const { _id, name, description, price, isActive} = productProp;
	const [isReleased, setIsReleased] = useState(isActive)

	const {user} = useContext(UserContext)

	const archiveProduct = () => {

		fetch(`https://serene-thicket-42677.herokuapp.com/products/${_id}/archive`, {
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})

		setIsReleased(false);

	}

	const activateProduct = () => {

		fetch(`https://serene-thicket-42677.herokuapp.com/products/${_id}/activate`, {
			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})

		setIsReleased(true);
	}

	return(
		<Fragment>
			<div className='productcard-container productcard-background productcard-scale'>
				<h3>{name}</h3>
				<p>{description}</p>
				<h4>&#8369;{price}</h4>
				{
					(user.id ===  null || user.isAdmin === false) ?
						<Button variant="primary" as={Link} to={`/products/${_id}`}>View</Button>
						:
						<Fragment>
							<div className='admin-button'>
								<Button variant="primary" as={Link} to={`/products/${_id}`}>Edit</Button>
								{
									(isReleased) ?
										<Button variant="danger" onClick={() => archiveProduct()}>Archive</Button>
										:
										<Button variant="primary" onClick={() => activateProduct()}>Activate</Button>
								}
							</div>
						</Fragment>
				}
			</div>		
		</Fragment>
	)

}

export default ProductCard;