//React Main Package
import React, { Fragment, useState, useEffect, useContext } from 'react';

//React Router Package
import { Link } from 'react-router-dom';

//React Bootstrap Package
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

//Global Context Consumer
import UserContext from './UserContext';

//CSS Module
import './AppNavbar.css';



const AppNavbar = () => {

	const {user} = useContext(UserContext);

	return(
		<Navbar expand="lg" className='appnavbar-background'>
		    <Navbar.Brand as={Link} to="/" className="brand-style appnavbar-background onhover">Gamer<span>Me</span></Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav" className="d-lg-flex justify-content-end appnavbar-background">
				<Nav className="nav-container">
					<Nav.Link className='appnavbar-background onhover' id='text-color' as={Link} to="/">Home</Nav.Link>
					<Nav.Link className='appnavbar-background onhover' id='text-color' as={Link} to="/products">Products</Nav.Link>
					{
						(user.id !== null) ?
							(user.isAdmin === true) ?
								<Nav.Link className='appnavbar-background text-color' id='text-color' as={Link} to="/logout">Logout</Nav.Link>
								:
								<Fragment>
									<Nav.Link className='appnavbar-background onhover' id='text-color' as={Link} to="/cart">My Cart</Nav.Link>
									<Nav.Link className='appnavbar-background onhover' id='text-color' as={Link} to="/myOrders">My Orders</Nav.Link>
									<Nav.Link className='appnavbar-background onhover' id='text-color' as={Link} to="/logout">Logout</Nav.Link>
								</Fragment>
							:
							<Fragment>
								<Nav.Link className='appnavbar-background onhover text-color' id='text-color' as={Link} to="/login">Login</Nav.Link>
								<Nav.Link className='appnavbar-background onhover text-color' id='text-color' as={Link} to="/register">Register</Nav.Link>
							</Fragment>
					}
		        </Nav>
		    </Navbar.Collapse>
		</Navbar>
	)
}

export default AppNavbar;