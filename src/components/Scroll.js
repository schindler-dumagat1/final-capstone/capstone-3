import React from 'react';

const Scroll = (props) => {

	return(
		<div style={{overflow: 'auto', border: '5px solid #008DBE', height: '500px', margin: '0px 10px', padding: '20px'}}>
			{props.children}
		</div>
	)

}

export default Scroll