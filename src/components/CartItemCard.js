import React, { Fragment, useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import './CartItemCard.css'

const CartItemCard = ({cartItemProp, cartStatusProp}) => {
	
	const { _id, name, description, price, quantity, subTotal } = cartItemProp;
	const { editQuantity, removeItem, isSelected, setIsSelected } = cartStatusProp;

	const [number, setNumber] = useState(quantity);

	const [firstLoad, setFirstLoad] = useState(true)
	const [isChosen, setIsChosen] = useState(false)

	useEffect(() => {
		if(isChosen){
			setIsSelected({chosen: isChosen, id: _id, subTotal: subTotal})
		} else {
			if(!firstLoad) {
				setIsSelected({chosen: isChosen, id: _id, subTotal: subTotal})
			}
		}
		setFirstLoad(false)
	}, [isChosen])


	return(

		<Fragment>
			<div className='cartitemcard-item'>
				<input type='checkbox' onChange={() => setIsChosen(!isChosen)} />
				<h3>{name}</h3>
				<p>{description}</p>
				<h4>Price: &#8369;{price}</h4>
				{
					(isChosen) ?
						<input disabled value={number} type="number" onChange={(e) => setNumber(e.target.value)} />
						:
						<input value={number} type="number" onChange={(e) => setNumber(e.target.value)} />
				}
				<h4>Subtotal: &#8369;{subTotal}</h4>
				<div className='cartitemcard-button-container'>
					<Button onClick={() => editQuantity(_id, number, price, subTotal)}>Save</Button>
					<Button onClick={() => removeItem(_id)}>Remove</Button>
				</div>
			</div>
		</Fragment>

		
	)

}

export default CartItemCard