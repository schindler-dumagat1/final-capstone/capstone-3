import React, { Fragment, useState, useEffect } from 'react';

import OrderGroup from '../components/OrderGroup'

import Scroll from '../components/Scroll'

import './OrderHistory.css'

const OrderHistory = () => {

	const [orderGroups, setOrderGroups] = useState([]);

	useEffect(() => {

		fetch('https://serene-thicket-42677.herokuapp.com/users/myOrders', {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setOrderGroups(data.map(group => {
				return (<OrderGroup key={data.indexOf(group)} groupProp={group} />)
			}))
		})

	}, [])

	return(
		<Fragment>
			<h1 className='text-center'>My Orders</h1>
			<Scroll>
				<div className='sortedOrder'>
					{orderGroups}
				</div>
			</Scroll>
		</Fragment>
	)

}

export default OrderHistory;