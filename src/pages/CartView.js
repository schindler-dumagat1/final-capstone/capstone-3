import React, { Fragment, useState, useEffect, useContext } from 'react'

import { Navigate, Link } from "react-router-dom";

import Button from 'react-bootstrap/Button';

import UserContext from '../components/UserContext';

import CartItemCard from '../components/CartItemCard'

import Scroll from '../components/Scroll'

import Swal from 'sweetalert2';

import './CartView.css'

const CartView = () => {

	const [cart, setCart] = useState([]);
	const [stat, setStat] = useState("")
	let [total, setTotal] = useState(0);
	const [isSelected, setIsSelected] = useState({chosen: false, id: null, subTotal: 0})

	const [itemIds, setItemIds] = useState([]);
	const [stat2, setStat2] = useState("");
	
	const {user} = useContext(UserContext);

	useEffect(() => {

		if(isSelected.chosen) {
			itemIds[itemIds.length] = isSelected.id
			setTotal(total += isSelected.subTotal)
		} else {
			itemIds.splice(itemIds.indexOf(isSelected.id), 1)
			setTotal(total -= isSelected.subTotal)
			
		}
		if(total < 0) {
			setTotal(0)
		}
		setStat2(JSON.stringify(itemIds))

	}, [isSelected.chosen, isSelected.id])

	useEffect(() => {
		fetch('https://serene-thicket-42677.herokuapp.com/users/cart', {
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res =>  res.json())
		.then(data => {	
			setCart(data.map(item => {
				return <CartItemCard key={item._id}
									 cartItemProp={item} 
									 cartStatusProp={{	editQuantity: editQuantity,
									 					removeItem: removeItem,
									 					// selectItem: selectItem
									 					isSelected: isSelected,
									 					setIsSelected: setIsSelected
									 					}} 
									 />
			}))
			setStat(JSON.stringify(cart))
		})
		
	}, [stat, total])

	const editQuantity = (id, number, price, subTotal) => {
		fetch(`https://serene-thicket-42677.herokuapp.com/users/cart/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				quantity: number
			})
		})
		.then(res => res.json())
		.then(data => {
			setStat(JSON.stringify(data))
		})
	}

	const removeItem = (id) => {
		fetch(`https://serene-thicket-42677.herokuapp.com/users/cart/${id}`, {
			method: 'DELETE',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setStat(JSON.stringify(data))
		})
	}
	const saveOrders = () => {
		// console.log(JSON.parse(stat2))
		fetch('https://serene-thicket-42677.herokuapp.com/users/checkout', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				itemId: JSON.parse(stat2)
			})
		})
		Swal.fire({
		  icon: 'success',
		  title: 'Purchase Successful',
		  text: 'Please check you order history'
		})
	}

	return(

		<Fragment>
		{
			(!user.isAdmin) ?
				<Fragment>
					<h1 className='text-center'>My Cart</h1>
					<Scroll>
						<div className='cartview-grid'>
							{cart}
						</div>
						<div className='mt-2'>
							<h1>Total Price: <span>{total}</span></h1>
							<div className='cartview-button'>
								<Button as={Link} to='/products'>Go Back</Button>
								<Button as={Link} to='/myOrders' onClick={() => saveOrders()}>Checkout</Button>
							</div>
						</div>
					</Scroll>
				</Fragment>
				:
				<Navigate to='/products' />
		
		}
		</Fragment>

	)

}

export default CartView;