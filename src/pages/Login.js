//React Main Package
import React, {useState, useEffect, useContext} from 'react';

//React DOM Package
import { Navigate } from "react-router-dom";

//React Bootstrap Package
import { Container, Row, Col } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

//Global Context Consumer
import UserContext from '../components/UserContext';

import './Login.css'

//Other Packages
import Swal from 'sweetalert2';



const Login = () => {

	//States
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isAllSatisfied, setIsAllSatisfied] = useState(false);

	//Consumer Context
	const {user, setUser} = useContext(UserContext);

	//Effect
	useEffect(() => {

		if(email !== '' && password !== '') {
			setIsAllSatisfied(true);
		} else {
			setIsAllSatisfied(false);
		}

	}, [email, password])

	const authenticateUser = (e) => {

		e.preventDefault();

		fetch('https://serene-thicket-42677.herokuapp.com/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json' 
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data !== false) {

				localStorage.setItem('token', data.access);
				fetch('https://serene-thicket-42677.herokuapp.com/users/details', {
					headers: {
						'Authorization': `Bearer ${data.access}`
					}
				})
				.then(res => res.json())
				.then(data => {
					setUser({
						id: data._id,
						isAdmin: data.isAdmin
					})
				})
				Swal.fire({
				  icon: 'success',
				  title: 'Login Successful',
				  text: 'You have succesfully logged in you account!'
				})
			} else {
				Swal.fire({
				  icon: 'error',
				  title: 'Login Failed',
				  text: 'Wrong credentials detected.'
				})
			}
		})

	}

	return(

		(user.id !== null) ?
			<Navigate to='/products' />
			:
			<Form onSubmit={(e) => authenticateUser(e)}>
				<Container>
					<Row className="justify-content-center">
						<Col xs={12} md={8} lg={4}>
							<h1 className="text-center">Login</h1>
						  	<Form.Group className="mb-3" controlId="formBasicEmail">
						    	<Form.Label className='login-label'>Email address</Form.Label>
						    	<Form.Control type="email" value={email}  placeholder="Enter email" onChange={(e) => setEmail(e.target.value)} />
						  	</Form.Group>

						  	<Form.Group className="mb-3" controlId="formBasicPassword">
						    	<Form.Label className='login-label'>Password</Form.Label>
						    	<Form.Control type="password" value={password} placeholder="Enter password" onChange={(e) => setPassword(e.target.value)} />
						  	</Form.Group>

						  	<Form.Group className="mb-3" controlId="formBasicText">
				    		  	<Form.Text className="text-muted">
				    	  	  		We'll never share your details with anyone else.
				    	  		</Form.Text>	
						  	</Form.Group>
						  	<div className="text-center">
						  		{
						  			(isAllSatisfied) ?
					  					<Button variant="primary" type="submit" >
					  				  		Submit
					  					</Button>
					  					:
				  						<Button variant="primary" type="submit" disabled>
				  					  		Submit
				  						</Button>
						  		}
						  	</div>
						</Col>
					</Row>
			  	</Container>
			</Form>
	)
}

export default Login;
