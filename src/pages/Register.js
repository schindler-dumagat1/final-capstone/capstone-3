//React Main Package
import React, {useState, useEffect, useContext} from 'react';

//React DOM Package
import { Navigate } from "react-router-dom";

//React Bootstrap Package
import { Container, Row, Col } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';

//Global Context Consumer
import UserContext from '../components/UserContext';

//Other Packages
import Swal from 'sweetalert2'

//CSS Module
import './Register.css'

const Register = () => {
	
	//States
	const [email, setEmail] = useState("");
	const [mobileNo, setmobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isAllSatisfied, setIsAllSatisfied] = useState(false);
	const [isSuccessful, setIsSuccessful] = useState(false);

	//Consumer Context
	const {user} = useContext(UserContext);

	useEffect(() => {

		if((email !== '' && 
			mobileNo !== '' && 
			password1 !== '' && 
			password2 !== ''
			) && (
			mobileNo.length === 11 && 
			password1 === password2)) {

			setIsAllSatisfied(true)
		} else {
			setIsAllSatisfied(false)
		}

	}, [email, mobileNo, password1, password2]);

	const registerUser = (e) => {

		e.preventDefault();

		fetch('https://serene-thicket-42677.herokuapp.com/users/checkEmail', {
			method: 'POST',
			headers: {
		      'Content-Type': 'application/json'
		    },
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			if(!data) {
				fetch('https://serene-thicket-42677.herokuapp.com/users/register', {
					method: 'POST',
					headers: {
				      'Content-Type': 'application/json'
				    },
					body: JSON.stringify({
						email: email,
						mobileNo: mobileNo,
						password: password1
					})
				})
				setIsSuccessful(true);
				setEmail('');
				setmobileNo('');
				setPassword1('');
				setPassword2('');
				Swal.fire({
				  icon: 'success',
				  title: 'Registration Successful',
				  text: 'You Account is Successfully Registered!'
				})
			} else {
				Swal.fire({
				  icon: 'error',
				  title: 'Email Already Used',
				  text: 'Please Use Another Email'
				})
			}
		})

	}

	return(

		(isSuccessful) ?
			<Navigate to="/login"/>
			:
			(user.id !== null) ?
				<Navigate to="/products"/>
				:
				<Form onSubmit={(e) => registerUser(e)}>
					<Container>
						<Row className='justify-content-center'>
							<Col xs={12} md={8} lg={4}>
								<h1 className="text-center">Register</h1>

							  	<Form.Group className="mb-3" controlId="formBasicEmail">
							    	<Form.Label className='register-label'>Email address:</Form.Label>
							    	<Form.Control value={email} type="email" placeholder="Enter email" onChange= {(e) => setEmail(e.target.value)} />
							  	</Form.Group>

							  	<Form.Group className="mb-3" controlId="formBasicMobileNo">
						  	    	<Form.Label className='register-label'>Mobile Number:</Form.Label>
						  	    	<Form.Control value={mobileNo} type="text" placeholder="Enter mobile number" onChange= {(e) => setmobileNo(e.target.value)} />
						  	  	</Form.Group>

							  	<Form.Group className="mb-3" controlId="formBasicPassword1">
							    	<Form.Label className='register-label'>Password:</Form.Label>
							    	<Form.Control value={password1} type="password" placeholder="Enter Password" onChange= {(e) => setPassword1(e.target.value)} />
							  	</Form.Group>

							  	<Form.Group className="mb-3" controlId="formBasicPassword2">
							    	<Form.Label className='register-label'>Verify Password:</Form.Label>
							    	<Form.Control value={password2} type="password" placeholder="Verify Password" onChange= {(e) => setPassword2(e.target.value)} />
							  	</Form.Group>

							  	<Form.Text className="text-muted">
						  	  		We'll never share your details with anyone else.
						  		</Form.Text>

							  	<div className="text-center">
								  	{						
								  		(isAllSatisfied) ?
							  			<Button className='register-button' type="submit" className="mt-2">
							  		  		Submit
							  			</Button> 
							  			:
						  				<Button className='register-button' type="submit" className="mt-2" disabled>
						  			  		Submit
						  				</Button>
								  	}
							  	</div>
						  	</Col>
					  	</Row>
				  	</Container>
				</Form>
	)
}

export default Register;
