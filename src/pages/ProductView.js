import React, { Fragment, useState, useEffect, useContext } from 'react';

import Button from 'react-bootstrap/Button';

import { useParams, Link } from 'react-router-dom';

import UserContext from '../components/UserContext';

import CartView from './CartView'

import Swal from 'sweetalert2';

import './ProductView.css'

const ProductView = () => {

	const [product, setProduct] = useState({})
	const [quantity, setQuantity] = useState(1)
	const {user} = useContext(UserContext);
	const { id } = useParams();

	const {_id, name, description, price} = product;
	console.log(product);

	useEffect(() => {
		fetch(`https://serene-thicket-42677.herokuapp.com/products/${id}`)
		.then(res => res.json())
		.then(data => {
			setProduct(data);
		})
	}, [])

	const addToCart = () => {

		fetch('https://serene-thicket-42677.herokuapp.com/users/cart', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				"productId": _id,
			    "quantity": quantity
			})
		})
		Swal.fire({
		  icon: 'success',
		  title: 'Successfully Added to Cart',
		  text: 'Please check you cart'
		})

	}
	
	return(

		<Fragment>
			<div className='page-height'>
				<div className='container-background'>
					<h1>{name}</h1>
					<p>{description}</p>
					<h3>Price: &#8369;{price}</h3>
					<input type="number" value={quantity} onChange={(e) => setQuantity(e.target.value)} />
					<h3>Subtotal: &#8369;{price * quantity}</h3>
					<div className='button-container'>
						<Button variant="primary" as={Link} to='/products'>Go Back</Button>
						{
							(user.id !== null && user.isAdmin === false) ?
								<Button variant="primary" onClick={() => addToCart()}>Add To Cart</Button>
								:
								<Button variant="primary" as={Link} to='/login'>Login First</Button>
						}
					</div>
				</div>
			</div>
			
		</Fragment>

	)

}

export default ProductView;