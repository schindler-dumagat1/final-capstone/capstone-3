import React, { Fragment, useState, useEffect } from 'react'

import { useParams, Link, Navigate } from 'react-router-dom';

import Button from 'react-bootstrap/Button';

import Form from 'react-bootstrap/Form'

import './ProductEdit.css'

const ProductEdit = () => {

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState(false);

	const { id } = useParams();

	useEffect(() => {

		fetch(`https://serene-thicket-42677.herokuapp.com/products/${id}`)
		.then(res =>  res.json())
		.then(data => {
			const { name, description, price, isActive } = data;
			setName(name)
			setDescription(description)
			setPrice(price)
			setIsActive(isActive)
		})

	}, [])

	const editProduct = (e) => {

		e.preventDefault()

		fetch(`https://serene-thicket-42677.herokuapp.com/products/${id}`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				isActive: isActive,
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})

		// return(<Navigate to='/products' />)

	}

	return(
		<Fragment>
			<div className='form-edit-container'>
				<div className='form-container'>
					<Form onSubmit={(e) => editProduct(e)}>
					  <Form.Group className="mb-3" controlId="formBasicName">
					    <Form.Label>Name</Form.Label>
					    <Form.Control type="text" value={name} placeholder="Enter product name" onChange={(e) => setName(e.target.value)} />
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicDescription">
					    <Form.Label>Description</Form.Label>
					    <Form.Control type="text" value={description} placeholder="Enter product description" onChange={(e) => setDescription(e.target.value)} />
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicPrice">
					    <Form.Label>Price</Form.Label>
					    <Form.Control type="number" value={price} placeholder="Enter product price" onChange={(e) => setPrice(e.target.value)} />
					  </Form.Group>

					  <Form.Check 
					      type="switch"
					      id="custom-switch"
					      label="Activated?"
					      checked={isActive}
					      onChange={() => setIsActive(!isActive)}
					    />
					  <div className='edit-button'>
						  <Button as={Link} to='/products' variant="primary">
						    Go Back
						  </Button>
						  <Button variant="primary" type="submit">
						    Save Changes
						  </Button>
					  </div>
					</Form>
				</div>
			</div>
		</Fragment>
	)

}

export default ProductEdit