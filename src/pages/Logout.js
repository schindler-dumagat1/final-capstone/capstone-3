//React Main Package
import React, { useEffect, useContext } from 'react';

//React DOM Package
import { Navigate } from "react-router-dom";

//Global Context Consumer
import UserContext from '../components/UserContext';



const Logout = () => {

	//Consumer Context
	const { setUser, clearStorage} = useContext(UserContext);

	useEffect(() => {
		setUser({
			id: null,
			isAdmin: null
		})
		clearStorage()
	}, [])


	return(

		<Navigate to='/login' />

	)

}

export default Logout;