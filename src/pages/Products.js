//React Main Package
import React, { Fragment, useState, useEffect, useContext } from 'react';

//Components
import ProductCard from '../components/ProductCard'

import Button from 'react-bootstrap/Button';

import { Link } from 'react-router-dom';

import UserContext from '../components/UserContext'

import Scroll from '../components/Scroll'

import './Products.css'

const Product = () => {

	const [products, setProducts] = useState([]);

	const { user } = useContext(UserContext)


	useEffect(() => {
		console.log(user);
		if(user.id !== null && user.isAdmin === true) {
			fetch('https://serene-thicket-42677.herokuapp.com/products/all', {
				headers: {
					'Authorization': `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				setProducts(data.map(product => {
					return(<ProductCard key={product._id} productProp={product}/>)
				}))
			})
		} else {
			fetch('https://serene-thicket-42677.herokuapp.com/products/')
			.then(res => res.json())
			.then(data => {
				setProducts(data.map(product => {
					return(<ProductCard key={product._id} productProp={product}/>)
				}))
			})	
		}
		
	}, [user])

	return(

		<Fragment>
			{
				(user.id !== null && user.isAdmin === true) ?
					<Fragment>
						<h2 className='text-center'>Products</h2>
						<div className="products-container products-page-background">
								<Scroll>
									<div className='text-center mb-2'>
										<Button as={Link} to='/products/new'>Create Product</Button>
									</div>
									<div className='products-grid products-container-background'>	
										{products}
									</div>
								</Scroll>
						</div>
					</Fragment>
					:
					<Fragment>
						<div className='products-container products-page-background'>
							<h2 className='text-center'>Products</h2>
							<Scroll>
								<div className='products-grid products-container-background'>	
									{products}
								</div>
							</Scroll>
						</div>
					</Fragment>
			}
		</Fragment>

		)

}

export default Product;