//React Main Package
import React from 'react';

//React DOM Package
import { createRoot } from 'react-dom/client';

//React Bootstrap CSS 
import 'bootstrap/dist/css/bootstrap.min.css';

//Application Component
import App from './App';




const container = document.getElementById('root');
const root = createRoot(container);
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

