//React Main Package
import React, { useState, useEffect } from 'react';

//React DOM Package
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

//App Components
import AppNavbar from './components/AppNavbar';

//App Pages
import Home from './pages/Home';
import Products from './pages/Products';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import ProductView from './pages/ProductView';
import ProductCreate from './pages/ProductCreate';
import ProductEdit from './pages/ProductEdit';
import CartView from './pages/CartView';
import OrderHistory from './pages/OrderHistory';
import Error from './pages/Error';

//Global Context Provider
import { UserProvider } from './components/UserContext';

//CSS Module
import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const clearStorage = () => {
    localStorage.clear()
  }

  useEffect(() => {
    fetch('https://serene-thicket-42677.herokuapp.com/users/details', {
      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(data !== false) {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })

  }, [])

  return (
    <UserProvider value={{user, setUser, clearStorage}}>
      <Router>
        <AppNavbar/>
        <Routes>
          <Route exact path='/' element={<Home/>} />
          <Route exact path='/products' element={<Products/>} />
          <Route exact path='/login' element={<Login/>} />
          <Route exact path='/logout' element={<Logout/>} />
          <Route exact path='/register' element={<Register/>} />
          <Route exact path='/register' element={<Register/>} />
          <Route exact path='/products/new' element={<ProductCreate/>} />
          {
            (user.id !== null && user.isAdmin === true) ?
              <Route exact path='/products/:id' element={<ProductEdit/>} />
              :
              <Route exact path='/products/:id' element={<ProductView/>} />
          }
          <Route exact path='/cart' element={<CartView/>} />
          <Route exact path='/myOrders' element={<OrderHistory/>} />
          <Route exact path='*' element={<Error/>} />

        </Routes>
      </Router>
    </UserProvider>
  );
}

export default App;
